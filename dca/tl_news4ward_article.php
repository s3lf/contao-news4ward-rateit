<?php

/**
 * Extend tl_news4ward_article
 */
$GLOBALS['TL_DCA']['tl_news4ward_article']['config']['onsubmit_callback'][] = array('tl_news4ward_article_rating','insert');
$GLOBALS['TL_DCA']['tl_news4ward_article']['config']['ondelete_callback'][] = array('tl_news4ward_article_rating','delete');

/**
 * Palettes
 */
$GLOBALS['TL_DCA']['tl_news4ward_article']['palettes']['__selector__'][] = 'addRating';
$GLOBALS['TL_DCA']['tl_news4ward_article']['palettes']['default'] = $GLOBALS['TL_DCA']['tl_news4ward_article']['palettes']['default'].';{rating_legend:hide},addRating';

/**
 * Add subpalettes to tl_article
 */
$GLOBALS['TL_DCA']['tl_news4ward_article']['subpalettes']['addRating']  = 'rateit_position';

// Fields
$GLOBALS['TL_DCA']['tl_news4ward_article']['fields']['addRating'] = array
(
  'label'						=> &$GLOBALS['TL_LANG']['tl_news4ward_article']['addRating'],
  'exclude'						=> true,
  'inputType'					=> 'checkbox',
  'sql' 							=> "char(1) NOT NULL default ''",
  'eval'           		   => array('tl_class'=>'w50 m12', 'submitOnChange'=>true)
);

$GLOBALS['TL_DCA']['tl_news4ward_article']['fields']['rateit_position'] = array
(
  'label'                  => &$GLOBALS['TL_LANG']['tl_news4ward_article']['rateit_position'],
  'default'                => 'before',
  'exclude'                => true,
  'inputType'              => 'select',
  'options'                => array('after', 'before'),
  'reference'              => &$GLOBALS['TL_LANG']['tl_news4ward_article'],
  'sql' 							=> "varchar(6) NOT NULL default ''",
  'eval'                   => array('mandatory'=>true, 'tl_class'=>'w50')
);

class tl_news4ward_article_rating extends rateit\DcaHelper {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}
	
	public function insert(\DC_Table $dc) {
      return $this->insertOrUpdateRatingKey($dc, 'news4ward', $dc->activeRecord->title);
	}

	public function delete(\DC_Table $dc)
	{
      return $this->deleteRatingKey($dc, 'news4ward');
	}
}
