<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'cgoIT',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'cgoIT\rateit\RateItNews4ward' => 'system/modules/news4ward_rateit/classes/RateItNews4ward.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'mod_news4ward_reader_rateit'    => 'system/modules/news4ward_rateit/templates',
	'mod_news4ward_reader_rateit_md' => 'system/modules/news4ward_rateit/templates',
	'news4ward_list_item_rateit'     => 'system/modules/news4ward_rateit/templates',
	'news4ward_list_item_rateit_md'  => 'system/modules/news4ward_rateit/templates',
));
